### What it is ###

Python library for pre-/post-processing, analysing and visualising 
data input/output from dftb+ or lodestar.

--- MIT License ---

### Installation ###

RECOMMENDED for users: 
======================================================================
Install as user, locally: No root permissions required, no PATH/PYTHONPATH
modifications required. 

$ python? setup.py install --user --record installed.info

This will try to install skgen into your local home directory, creating
~/.local/lib/python?.[?]/site-packages/dftbutilsJ.I[.P[.S]].egg-info, 
~/.local/lib/python?.[?]/site-packages/dftbutils/. 

Currently there are a few executables associated with the library,
and they would appear in ~/.local/bin/

If there was any executable in ~/.local/bin/, it should be invoked with
the path, or sym-linked or ~/.local/bin/ added to PATH (the latter
is recommended).

All installed files will be listed in installed.info, so to uninstall do:
$ cat installed.info | xargs rm -rf


Option for admin/root-priviliged users e.g. installing on a cluster:
======================================================================
$ python? setup.py install

which will install on /usr/local/,

or with a prefix:

$ python setup.py install --prefix=somewhere_else



Other ways:
======================================================================
Check http://docs.python.org/install/ for more information and understanding.

* How to use:
  To use the library: from dftbutils.? import ??
  Check out the executables in the ./bin directory too.

* Configuration
  No configurations, but in the future, a dftbutils.rc file or 
  something like that may be provided where options like:
  dftbutils.exe = 'lodestar' # or 'dftb+'
  may be used to customise the behaviour of sertain routines
  related to the format of data output by the corresponding dftb code.

* Dependencies:   
  numpy, scipy, matplotlib, mayavi, ase, libspg  

* Database configuration
  None for the moment

* How to run tests
  todo: python unit test?

* Deployment instructions
  todo: provide examples

### Contribution guidelines ###

* Writing tests

* Code review

* Other guidelines

### Who do I talk to? ###

* Repo owner or admin:  
  Stanislav Markov (figaro@hku.hk)

* Other community or team contact