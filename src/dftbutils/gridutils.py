"""
Functions and class related to aquiring info of the structured 
rectilinear grid output from dftb+ and obtaining specific information about
the simulation domain.
Further routines for obtaining vtk grid objects, or mesh files for other 
visualisation programs.

author: Stanislav Markov (fiaro@hku.hk)
created: 28 Aug. 2014
"""

import os, sys
import numpy as np
from tvtk.api import tvtk

def get_grid(workdir, xfile='Xvector.dat',
             yfile='Yvector.dat', zfile='Zvector.dat',
             nheaderlines=0):
    """ 
    Fetch the device grid, assuming [XYZ]vector.dat are in workdir
    Workdir is where the [XYZ]vector.dat files are. The files may have different
    names, as is the case for the Contacts directory, where we have, e.g. 
    Xvector_01.dat for contact 1.
    nheaderlines gives us further flexibility, if there is a number of lines
    as a header before the actual positions data.
    """
    xfile = os.path.join(workdir, xfile)
    yfile = os.path.join(workdir, yfile)
    zfile = os.path.join(workdir, zfile)
    xx = np.loadtxt(xfile, skiprows=nheaderlines)
    yy = np.loadtxt(yfile, skiprows=nheaderlines)
    zz = np.loadtxt(zfile, skiprows=nheaderlines)
    return xx, yy, zz


class Grid3D(object):
    """
    A class holding the grid information.
    Provide access to compound, as well as separated info, 
    e.g. self.vectors = [self.xx,self.yy,self.zz] to facilitate
    access.
    TODO:
    Ideally, it should have some routines to handle automatic unit
    conversion, e.g. if we set unit to 'nm', the position related
    data should all be transformed in the new unit. not sure how to do...
    """

    def __init__(self, workdir, xfile='Xvector.dat',
                 yfile='Yvector.dat', zfile='Zvector.dat',
                 unit='Angstrom', nheaderlines=0):
        self.workdir = workdir
        self.unit = unit

        # get grid positions from files
        self.xx, self.yy, self.zz = \
            get_grid(self.workdir, xfile, yfile, zfile, nheaderlines)
        self.vectors = [self.xx, self.yy, self.zz]

        # calculate dimensions in each directions
        self.nx, self.ny, self.nz = len(self.xx), len(self.yy), len(self.zz)
        self.dimensions = (self.nx, self.ny, self.nz)

        # calculate range and bounds
        self.xmin, self.xmax = min(self.xx), max(self.xx)
        self.ymin, self.ymax = min(self.yy), max(self.yy)
        self.zmin, self.zmax = min(self.zz), max(self.zz)
        self.xextent = self.xmax - self.xmin
        self.yextent = self.ymax - self.ymin
        self.zextent = self.zmax - self.zmin
        self.extent = (self.xextent, self.yextent, self.zextent)

        # midpoint of the grid box
        self.xmid = (self.xmin + self.xmax) / 2.
        self.ymid = (self.ymin + self.ymax) / 2.
        self.zmid = (self.zmin + self.zmax) / 2.

    def vtk(self, gridtype='rectilinear'):
        """
        Return a vtk grid object, initialised with the
        coordinates and dimensions of self.
        By default, the object will be RectiLinear grid.
        Structured grid can also be selected, but this is slower to visualize.
        """
        if gridtype.lower() == 'rectilinear':
            vtkgrid = self.vtk_rectilinear()

        if gridtype.lower() == 'structured':
            vtkgrid = self.vtk_structured()

        if gridtype.lower() not in ['rectilinear', 'structured']:
            print ('ERROR !!! Only rectilinear and structured grid vtk objects can be returned!')
            print ('          Recall that Poisson grid in DFTB+ is orthogonal, maybe unevenly spaced.')
            sys.exit()

        return vtkgrid

    def vtk_rectilinear(self):
        """
        Return a vtk rectilinear grid object, initialised with the
        coordinates and dimensions of self.
        """

        rg = tvtk.RectilinearGrid()
        rg.x_coordinates = self.xx
        rg.y_coordinates = self.yy
        rg.z_coordinates = self.zz
        rg.dimensions = self.dimensions

        return rg

    def vtk_structured(self):
        """
        Return a vtk structured grid object, initialised with the
        coordinates and dimensions of self.
        """
        points = []

        # note in vtk the first index varies fastest
        for iz in xrange(self.nz):
            for iy in xrange(self.ny):
                for ix in xrange(self.nx):
                    pt = (self.xx[ix], self.yy[iy], self.zz[iz])
                    points.append(pt)

        sg = tvtk.StructuredGrid(dimensions=self.dimensions, points=points)

        return sg

    def mesh(self):
        """
        Return a meshed-out grid, based on numpy.meshgrid, which
        can be conveniently used to create a datasource for visualisation
        in mayavi2 (mlab)
        NOTABENE: numpy.meshgrid indexing must be 'ij', so that we get
        X, Y, Z as needed, instead of Y,X,Z, (returned with the
        default indexing 'xy'.
        """
        x, y, z = np.meshgrid(self.xx, self.yy, self.zz, indexing='ij')
        return x, y, z

    def mesh2d(self, normal='z'):
        """
        Return the a 2D meshed-out grid, based on numpy.meshgrid, which
        can be conveniently used to create a datasource for surface
        visualisation in mayavi2 (mlab)
        NOTABENE: numpy.meshgrid indexing must be 'ij', so that we get
        X, Y as needed, instead of Y,X (returned with the
        default indexing 'xy'.
        """
        directions = ['x', 'y', 'z']
        # TODO: assertion to check dimension is in 0,1,2 or in directions
        if normal in directions:
            nn = directions.index(normal)
        else:  # assume normal in [0,1,2]
            nn = normal
        plane = [i for i in [0, 1, 2] if i != nn]
        v0, v1 = np.meshgrid(self.vectors[plane[0]],
                             self.vectors[plane[1]], indexing='ij')
        return v0, v1


def get_Grid3D(devicedir, contdir=""):
    """
    Return Grid3D object based on the grid files in _devicedir_.
    Optionally, return two more Grid3D objects based on the 
    contat-grid info in _contdir_, if specified.
    """
    griddev = Grid3D(devicedir)
    print('\tLengths of x, y, z vectors are {0}, {1}, {2}'.format(*griddev.dimensions))
    print ('\tDevice length (along Z) is {0:5} nm, stretching from {1} to {2}'.
        format(griddev.zextent/10, griddev.zmin/10, griddev.zmax/10))
    print ('\tDevice height (along Y) is {0:5} nm, stretching from {1} to {2}'.
        format(griddev.yextent/10, griddev.ymin/10, griddev.ymax/10))
    print ('\tDevice width  (along Y) is {0:5} nm, stretching from {1} to {2}'.
        format(griddev.xextent/10, griddev.xmin/10, griddev.xmax/10))

    if contdir:
        gridcont1 = Grid3D(contdir, 'Xvector_01.dat', 'Yvector_01.dat', 'Zvector_01.dat')
        gridcont2 = Grid3D(contdir, 'Xvector_02.dat', 'Yvector_02.dat', 'Zvector_02.dat')
        return griddev, gridcont1, gridcont2
    else:
        return griddev
