"""
Module with a few routines for creating a file with Dirichlet BC
for a Poisson box, by specifying 'gates'
"""
import sys
import os
import logging

def gate_planar(poisson_box, poisson_npts, dimensions, position, dirl='x', dirn='z',
                logger=None):
    """
    Assume equidistand grid for each direction.
    Orientation is defined by assuming Lg is along X, Hg is along normal
    """
    directions = {"x":1, "+x":1, "-x":-1, "+y":2, "y":2, "-y":-2 , "+z":3, "z":3, "-z":-3}

    ilen = directions[dirl]
    ihei = directions[dirn]
    iwid = [d for d in [1, 2, 3] if d not in [abs(ilen), abs(ihei)]][0]

    msg = "Directions: longitudinal {}, transverse {}, normal {}".format(ilen, iwid, ihei)
    if logger is None:
        print (msg)
    else:
        logger.info(msg)

    npts  = poisson_npts
    delta = [(poisson_box[1][i]-poisson_box[0][i])/(npts[i]-1) for i in range(3)]

    try:
        Lg = dimensions['L']
    except KeyError:
        Lg = poisson_box[1][abs(ilen)-1]-poisson_box[0][abs(ilen)-1]
    try:
        Wg = dimensions['W']
    except KeyError:
        Wg = poisson_box[1][abs(iwid)-1]-poisson_box[0][abs(iwid)-1]
    try:
        Hg = dimensions['H']
    except KeyError:
        if ihei > 0:
            Hg = poisson_box[1][abs(ihei)-1]-position[abs(ihei)-1]
        else:
            Hg = abs(poisson_box[0][abs(ihei)-1]-position[abs(ihei)-1])

    gate_box = [[0]*3, [0]*3]
    gate_box[0][abs(ilen)-1] = position[abs(ilen)-1] + (-Lg/2.)
    gate_box[1][abs(ilen)-1] = position[abs(ilen)-1] + (+Lg/2.)
    gate_box[0][abs(iwid)-1] = position[abs(iwid)-1] + (-Wg/2.)
    gate_box[1][abs(iwid)-1] = position[abs(iwid)-1] + (+Wg/2.)
    if ihei > 0:
        gate_box[0][abs(ihei)-1] = position[abs(ihei)-1]
        gate_box[1][abs(ihei)-1] = position[abs(ihei)-1] + Hg
    else:
        gate_box[0][abs(ihei)-1] = position[abs(ihei)-1] - Hg
        gate_box[1][abs(ihei)-1] = position[abs(ihei)-1]

    msg = "Gate box is:\n{:s}".format("\n".
            join(["  ".join(["{:>7.3f}".format(r) for r in pt]) for pt in gate_box]))
    if logger is None:
        print (msg)
    else:
        logger.info(msg)
    
    return gate_box
    

class Gate (object):
    """
    """
    supportedtypes = ["planar", "wrap.orthogonal", "wrap.circular"]
    
    def __init__(self, poisson_box, poisson_npts, gatetype=None,
            dimensions=None, position=None, dirl='x', dirn='z', logger=None):
        
        if gatetype is None or gatetype not in self.supportedtypes:
            logger.info("The following gate types are supported:")
            logger.info("\t\t".join(self.supportedtypes))
            logger.info("Dimensions and position must also be defined.")
            sys.exit()

        if gatetype == "planar":
            self.box =  gate_planar(poisson_box, poisson_npts, dimensions, position,
                                    dirl=dirl, dirn=dirn, logger=logger)


def isinside(pt, box):
    """
    return true if pt coordinates are within box
    """
    return all([box[0][i] <= pt[i] <= box[1][i] for i in range(3)])


def write_DBC (poisson_box, poisson_npts, gates, fp, append=False):
    """
    For all points that lie in the gates.box write x y z potential
    """
    fname = isinstance(fp, str)
    if fname:
        if append:
            fp = open(fp, "a")
        else:
            fp = open(fp, "w")

    npts  = poisson_npts
    pt0, pt1 = poisson_box[0], poisson_box[1]
    delta = [(pt1[i]-pt0[i])/(npts[i]-1) for i in range(3)]

    lines = []

    for gate in gates:
        for ix in xrange(npts[0]):
            x = pt0[0] + delta[0]*ix
            for iy in xrange(npts[1]):
                y = pt0[1] + delta[1]*iy
                for iz in xrange(npts[2]):
                    z = pt0[2] + delta[2]*iz
                    if isinside((x, y, z), gate.box):
                        lines.append("{}  {}  {}  {}".format(x, y, z, gate.potential))

    fp.writelines("\n".join(lines))

    if fname:
        fp.close()




