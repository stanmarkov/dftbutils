"""
Utilities to help with preparation of atomic structures,
based on the ASE.
"""
import os,sys
try:
    import ase
except ImportError:
    print ('Cannot import ASE and cannot continue')
    print ('Please, use sys.path.append(''_path_to_ASE_'') to enable ASE import first')

#sys.path.append('/Users/smarkov/Applications/ase-3.6.0.2515/build/lib/')
#ys.path.append('/usr/lib/python2.6/site-packages/')

from ase.io import read,write
from ase.io.pov import get_bondpairs

def centerCell (cell,directions=None):
    """
        Center the atoms within the unit cell
        Optionally, select direction(s), e.g.
            ('x') or (0) or ('y','z') or (0,2)
    """
    if directions is None:
        directions = [0,1,2]
        
    for di in directions:
        cc = [a.position[di] for a in cell]
        cmin,cmax = min(cc),max(cc)
        atomicextent = cmax - cmin
        atomiccentre = (cmax+cmin)*.5
#        cellvec = cell.get_cell()[di][di]
        tvec= [0,0,0]
#        tvec[di] = -cmin + (cellvec-atomicextent)/2.
        tvec[di] = -atomiccentre
        cell.translate(tvec)
        
    return cell

def foldinCell (atoms, axes=None, cellvec=None):
    """
    Translate certain atoms so that all atoms fit in
    the given unit cell.
    Assume that the cell is centered arouind the Origin!
    """
    if axes is None:
        axes = range(3)

    if cellvec is None:
        cellvec = [atoms.get_cell()[i][i] for i in range(3)]

    for a in atoms:
        
        for axis in axes:
            
            while a.position[axis] > cellvec[axis]*0.5:
                a.position[axis] = a.position[axis] - cellvec[axis]
            
            while a.position[axis] < -cellvec[axis]*0.5:
                a.position[axis] = a.position[axis] + cellvec[axis]
    return atoms

