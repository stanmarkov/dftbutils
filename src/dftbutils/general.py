"""
Some general routines, unspecific but needed by the 
executables in the bin directory of the package
"""
import logging
import os

def setuplogger(workdir, name):
    """
    Logging at console and to debug file
    -------------------------------------------------------------------
    Default logging is set so that dbcbuild.debug.log gets all DEBUG
    messages and above severity. At the same time console gets INFO and
    above severity.
    """
    logging.basicConfig(level=logging.DEBUG,
            format='%(name)s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M',
            filename=os.path.join(workdir,'dbcbuild.debug.log'),
            filemode='w')
    logger=logging.getLogger(name)
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger(name).addHandler(console)
    return logger



