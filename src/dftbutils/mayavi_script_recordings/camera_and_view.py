# Recorded script from Mayavi2
from numpy import array
try:
    engine = mayavi.engine
except NameError:
    from mayavi.api import Engine
    engine = Engine()
    engine.start()
if len(engine.scenes) == 0:
    engine.new_scene()
# ------------------------------------------- 
scene = engine.scenes[0]
scene.scene.parallel_projection = False
scene.scene.parallel_projection = True
scene.scene.parallel_projection = False
scene.scene.parallel_projection = True
scene.scene.camera.position = [-244.25044713020327, 3.24249267578125e-05, 1.4962122268582791e-14]
scene.scene.camera.focal_point = [0.09955286979675293, 3.24249267578125e-05, 0.0]
scene.scene.camera.view_angle = 30.0
scene.scene.camera.view_up = [-6.123233995736766e-17, 1.0, 2.2204460492503131e-16]
scene.scene.camera.clipping_range = [236.21815473699573, 255.17570471549038]
scene.scene.camera.compute_view_plane_normal()
scene.scene.render()
scene.scene.camera.position = [-244.25044713020327, 3.2063094008388386, 16.832954123538467]
scene.scene.camera.focal_point = [0.09955286979675293, 3.2063094008388386, 16.832954123538453]
scene.scene.camera.view_angle = 30.0
scene.scene.camera.view_up = [-6.123233995736766e-17, 1.0, 2.2204460492503131e-16]
scene.scene.camera.clipping_range = [236.21815473699573, 255.17570471549038]
scene.scene.camera.compute_view_plane_normal()
scene.scene.render()
