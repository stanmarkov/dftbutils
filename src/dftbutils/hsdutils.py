"""
provides a few template expressions that may be formatted
into a valid hsd file for inclusion by dftb_in.hsd
Includes:
    CustomReferenceOcc
    TODO: Analysis -- on atom projected DOS
"""

hsd_refocc_open=\
"""\
  CustomReferenceOcc {
"""


hsd_refocc_tmpl =\
"""\
    ReferenceOccupation {
      Atoms = %(atom)i
      %(orb)s = %(occ)f
    } 
"""


hsd_refocc_close=\
"""\
  } # CustomReferenceOcc
"""

hsd_onatomdos_tmpl =\
"""\
    Region {
        Atoms = %(atom)i   # %(comment)s
        Label = \"dos_%(symb)s.a.%(atom)i\"
    } 
"""
