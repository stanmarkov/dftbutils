"""
Functions to help define longitudinal variation in the 
doping profile of a device (mosfet), assuming 
1) uniformly doped source/drain regions (Nsd)
2) spacer (Lsp)
3) gate length (Lg)
4) and exponential or gaussian rolloff of 
   (sigma) nm/decade.
"""
import numpy as np
from dftbutils.hsdutils import hsd_refocc_tmpl

def exprolloff (distance, n0, sigma):
    """
    Returns exponentially decaying concentration versus distance,
    with a characteristic roll-off of sigma nm per decade.
    n0 is the initial concentration at distance == 0.
    """
    return np.exp(np.log(10.)*(np.asarray(distance)/(-sigma) + np.log10(n0)))


def gaussianrolloff (distance, n0, sigma, LL):
    """
    Returns gaussian concentration rolloff versus distance,
    with a characteristic roll-off of sigma nm per decade.
    The gaussian is centered at LL, which should nominally
    be Lg/2+Lsp or the inverse of that.
    The gaussian peaks to n0.
    """
    return n0 * np.exp(-((np.asarray(distance) - LL)**2)/(2 * sigma**2))


def exponentialprofile (xx, Lg, Lsp, sigma, Nsd):
    """
    Rerturns the longitudinal doping profile of a mosfet,
    assuming Lg, Lspacer and exponential doping rolloff sigma,
    with uniform source/drain doping Nsd, and symmetry
    of the device around xx==0.
    """
    xx = np.asarray(xx)
    LL = Lg/2. + float(Lsp)
    conc = np.zeros(len(xx))
    conc[xx>-LL] = exprolloff(xx[xx>-LL]+LL, Nsd, sigma)
    conc[xx<+LL] = conc[xx<+LL]+\
                 exprolloff(-xx[xx<+LL]+LL, Nsd, sigma)
    conc[xx<=-LL] = Nsd
    conc[xx>=+LL] = Nsd
    return conc


def gaussianprofile (xx, Lg, Lsp, sigma, Nsd):
    """
    Rerturns the doping longitudinal doping profile of a mosfet,
    assuming Lg, Lspacer and gaussian doping rolloff sigma,
    with uniform source/drain doping Nsd and symmetry
    of the device around xx=0
    Note that xx may be a list, so we convert on the fly to 
    array, when making up a boolean index mask.
    """
    xx = np.asarray(xx)
    LL = Lg/2. + float(Lsp)
    conc = np.zeros(len(xx))
    conc = gaussianrolloff(xx, Nsd, sigma, +LL)
    conc = conc + gaussianrolloff(xx, Nsd, sigma, -LL)
    conc[xx<=-LL] = Nsd
    conc[xx>=+LL] = Nsd
    return conc


def atomicdensity (Natoms, cellVol, atomicMass=None):
    """
    Return the atomic or mass density,
    depending on whether the atomicMass is
    provided as a kwarg or not, in units of
    the 1/volume or mass/volume.
    atomic density is Natoms/cellVol
    massdensity is atomicMass*Natoms/cellVol
    Natoms is number of atoms per cell
    cellVol is volume of the cell
    """
    if atomicMass is None:
        rho = Natoms/cellVol
    else:
        rho = atomicMass*Natoms/cellVol
    return rho


def write_doping_hsd(refocc, orbix, ix, atrange):
    """
    """
    rng = [rr for rr in atrange.keys()]
    hsd_data = dict([(rr,[]) for rr in rng])
    
    for rr in rng:
        # note that ix is constructed from ASE => 
        # atom indexes start from 0.
        data = atrange[rr]
        assert len(data) == 3
        firstatom, lastatom, rangefile = data

        # loop over the current range of atoms
        for ai in range(firstatom, lastatom+1): # recall range(a,b) is exclusive of b
            try:
                # ii will be assigned only if atom is doped
                # Note that ix is constructed from ASE => 
                # atom indexes start from 0, but first|lastatom come from
                # dftb_in.hsd => start from 1;
                # the -1 below corrects for that
                ii = ix.index(ai-1)
            except ValueError:
                # this happens if ai not in ix, i.e. atom is not doped
                continue
            occ = refocc[ii]
            orb = orbix[ii]
            # Note that ix is constructed from ASE => atom indexes start from 0.
            # But in the hsd the atoms must be indexed as 
            # per dftb+ and fortran => atom indexes start from 1 (hence the +1 below)
            # We also subtract the first index in the range, so that
            # we always have device and contact atom indexing start at 1
            atom = ai - firstatom + 1
            
            # add to the list the hsd expression for the current doped atom
            hsd_data[rr].append(hsd_refocc_tmpl%({'atom':atom,'orb':orb,'occ':occ}))
            
        # write the hsd for the current range of atoms
        with open(rangefile,'w') as f:
            f.write(("# Include this file in dftb_in.hsd using <<+ "
                     "in a CustomReferenceOcc block\n"))
            f.write(''.join(hsd_data[rr]))


def write_doping_lodestar(refocc, ix, atrange):
    """
    *refocc* is an array with reference occupancies, one float per atom in *ix*
    *ix* is a list with indexes of all atoms whose occuppancy is to be overridden
    *atrange* is an atomrange is a dictionary, whose keys are range names;
              and data is 3-tuple; e.g. "source": (first_atom, last_atom, doping_file)
    """
    # assuming  a simple file in two-column format: col1=atom index, col2=ref.occuppancy
    lodestar_doping_tmpl = "%(atom)i  %(occ)f\n"
    assert len(refocc) == len(ix)
    rng = [rr for rr in atrange.keys()]  # build a list of the names of ranges, e.g. source, drain, device
    doping_data = dict([(rr,[]) for rr in rng]) # perpare output dictionary with same keys
    
    for rr in rng:
        # note that ix is constructed from ASE => 
        # atom indexes start from 0.
        data = atrange[rr]
        assert len(data) == 3
        firstatom, lastatom, rangefile = data

        # loop over the current range of atoms
        for ai in range(firstatom, lastatom+1): # recall range(a,b) is exclusive of b
            try:
                # ii will be assigned only if atom is doped
                # Note that ix is constructed from ASE => 
                # atom indexes start from 0, but first|lastatom come from
                # dftb_in.hsd => start from 1;
                # the -1 below corrects for that
                ii = ix.index(ai-1) # find which index in refocc and ix corresponds to atom ia
            except ValueError:
                # this happens if ai-1 not in ix, i.e. atom is not doped
                continue
            occ = refocc[ii]
            # orb = orbix[ii] lodestar does not support orbital resolved charges
            # Note that ix is constructed from ASE => atom indexes start from 0.
            # But in the hsd the atoms must be indexed as 
            # per dftb+ and fortran => atom indexes start from 1 (hence the +1 below)
            # We also subtract the first index in the range, so that
            # we always have device and contact atom indexing start at 1
            atom = ai - firstatom + 1
            
            # add to the list the hsd expression for the current doped atom
            doping_data[rr].append(lodestar_doping_tmpl%({'atom':atom, 'occ':occ}))
            
        # write the hsd for the current range of atoms
        with open(rangefile,'w') as f:
#            f.write(("# Include this file in dftb_in.hsd using <<+ "
#                     "in a CustomReferenceOcc block\n"))
            f.write(''.join(doping_data[rr]))
