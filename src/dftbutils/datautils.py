"""
Routines for obtaining and manipulating data from the files
output of dftb+.

author: Stanislav Markov (fiaro@hku.hk)
created: 28 Aug. 2014
"""

import numpy as np
import os
from subprocess import call

from scipy.integrate import simps

from ase.units import Bohr
from ase import Atoms

def fexists(datafile):
    """
    Check if file exists, return true if so.
    Alternatively, check 'datafile.xz' is present, and unzip it.
    Give-up otherwise.
    """
    if os.path.isfile(datafile):
        return True
    else:
        archfile = datafile+".xz"
        if os.path.isfile(archfile):
            call(["xz", "-d", archfile])
            assert os.path.isfile(datafile)
            print ("Unzipped {} to {}".format(archfile, datafile))
            return True
        else:
            print ("ERROR: Neither {} nor {} does exist.".format(datafile, archfile))
            return False


def getData (datafile, griddim, scale=1.0, skiprows=0, dataorder='C'):
    """ 
    Open *datafile* and read in the data.
    Return a 3D np.array object of the data, multiplied by *scale*.
    The shape of the array is dictated by the dimensions of the grid,
    as specified in *griddim*. 
    *nheaderlines* (default is 0) are skipped from the start of *datafile*.
    NOTABENE: assumes C ordering of the data, i.e.
        last/first index changes fastest/slowest, and is based on 
        numpy.loadtxt; this can be changed by *dataorder*
    NOTABENE: we lock the array so once data is read from the files, it cannot
        be further modified
    """
    assert fexists(datafile)
    data = np.loadtxt(datafile, skiprows=skiprows)
    data = np.reshape(data, griddim, order=dataorder)
    data.setflags(write=0)
    return data*scale


def averageData (data, volume, grid, axes):
    """
    Return the average of *data* as volume integral divided by the *volume*.
    For 1D and 2D, the _volume_ is actually a line and area respectively,
    so line and surface integrals are taken.
    *grid* should be a list with the positional arrays for the directions 
    of averaging (integration), in the order xx, yy, zz.
    *axes* should be one of 'x', 'y', 'z' or 'xy', 'xz', 'yz', or 'xyz'.
    Uses simpson integration from scipy.
    """
    axesdict = {"x": (0,), "y": (1,), "z": (-1,), 
                "xy": (1, 0), "xz": (-1, 0) , "yz": (-1, 1),
                "xyz": (-1, 1, 0)}

    temp = np.asarray(data)
    for aa in axesdict[axes]:
        temp = simps(temp, np.asarray(grid[aa]), axis=aa)
    return temp/volume


def getLine (grid, data, dimension, position):
    """
    Assume *data* is mapped on *grid* (Grid3D object from gridutils.py) 
    and return a line of data along the specified *dimension* 
    ('x','y' or 'z', or 0, 1, 2) and grid line that is closest to 
    the transverse *position* specified as a tupple; 
    NOTABENE: 
        the *position* tupple is assumed to be either 
        yz, xz, or xy
    return the line of data, and the actual transverse position
    and the corresponding cut indexes
    NOTABENE:
        return a COPY! of the extracted linedata, because the *data* array
        is probably mutable, and if later someone does change lindedata
        for whatever reason or by mistake, the data array itself will be
        changed. But we should avoid that by all means!
    """
    directions =  ['x','y','z']
    # TODO: assertion to check dimension is in 0,1,2 or in derections
    if dimension in directions:
        dim = directions.index(dimension)
    else: # assume dimension in [0,1,2]
        dim = direction
    # find the closest point on the trnasverse grid    
    transverse = [i for i in [0, 1, 2] if i != dim]
    icut = []
    for i, pos in enumerate(position):
        icut.append((np.abs(grid.vectors[transverse[i]]-pos)).argmin())
    # cut the line by fisrt transposing the data array
    # so that the interested direction is in first dimension, while
    # second and third dimensions are the transverse directions
    linedata = data.transpose(dim,*transverse)[:,icut[0],icut[1]]
    cut_pos = (grid.vectors[transverse[0]][icut[0]],\
               grid.vectors[transverse[1]][icut[1]])
    return linedata.copy(), cut_pos, icut

def repeatcubedata(x,y,z,data,reps):
    """
    NOTA BENE: this works for equidistant grid along each direction
    x,y,z are one dimensional array/lists with spacing
    data is the data extracted from a Cube file (array shaped nx,ny,nz)
    reps is a tuple: number of repetitions in x,y,z
    returns x,y,z extended as per the necessary repetitions,
    and the repeated data
    """
    assert len(x) == data.shape[0], "{0}, {1}".format( len(x), data.shape[0])
    assert len(y) == data.shape[1], "{0}, {1}".format( len(x), data.shape[0])
    assert len(z) == data.shape[2], "{0}, {1}".format( len(x), data.shape[0])
    nx,ny,nz = data.shape
    kx,ky,kz = reps
    nrx, nry, nrz = nx*kx, ny*ky, nz*kz
#    dx,dy,dz = x[1]-x[0], y[1]-y[0], z[1]-z[0]
    
    def repeat(x,kx):
        """
        repeat the x-mesh in the given direction kx times
        assuming equidistant grid, as is the case with .cube files.
        """
        nx = len(x)
        dx = x[1]-x[0]
        rx = np.arange(nx*kx)*dx + x[0]
        #for irep in range(kx):
        #    rx[irep*nx:(irep+1)*nx] = x + irep*nx*dx
        #print len(rx)
        return rx

    rx, ry, rz = repeat(x,kx), repeat(y,ky), repeat(z,kz)
    rdata = np.empty((nrx, nry, nrz))
    print rdata.shape
    for ikx in range(kx):
        for iky in range(ky):
            for ikz in range(kz):
                ix, iy, iz = ikx*nx + np.arange(nx),\
                             iky*ny + np.arange(ny), ikz*nz + np.arange(nz)
                #print ix.shape, iy.shape, iz.shape
                #print np.arange(nx).shape,np.arange(ny).shape,np.arange(nz).shape
                rdata[np.meshgrid(ix,iy,iz,indexing='ij')] =\
                    data[np.meshgrid(np.arange(nx),np.arange(ny),np.arange(nz),indexing='ij')]
    #print rdata.shape
    return rx, ry, rz, rdata


class CubeData (object):
    """
    Object representing the data contained in a Gaussian .cube file.
    """
    def __init__(self, fileobj):
        """
        initialise from a .cube file.
        """
        if isinstance(fileobj, str):
            fileobj = open(fileobj)
        readline = fileobj.readline
        
        # skip two lines of comments
        readline()
        readline()

        # get number of atoms and origin from line 3
        line = readline().split()
        natoms = int(line[0])
        origin = np.array([float(x) for x in line[1:]])

        # get grid on which the data is computed from lines 4-6
        scale = Bohr            # default units are Bohr
        uvec = np.empty((3, 3)) # unit vectors
        cell = np.empty((3, 3)) # unit cell of the atomic structure, implied from the grid
        shape = []
        for i in range(3):
            n, x, y, z = [float(s) for s in readline().split()]
            # if n is negative, then units are Ansgstrom
            if n<0:
                scale = 1.0     
                n = abs(n)
            shape.append(n)
            uvec[i] = np.array([x, y, z]) # convert to Angstr.
            cell[i] = n*uvec[i]
        
        # get the atomic structure
        numbers = np.empty(natoms, int)
        positions = np.empty((natoms, 3))
        for i in range(natoms):
            line = readline().split()
            numbers[i] = int(line[0])
            positions[i] = [float(s) for s in line[2:]]
    
        # update the values according to the scale units
        self.origin  = scale * origin
        self.unitvec = scale * uvec
        cell       *= scale
        positions  *= scale
        
        # prepare an ase.Atoms object; note that unit cell is only implied
        self.atoms = Atoms(numbers=numbers, positions=positions, cell=cell)

        self.data = np.array([float(s)
                            for s in fileobj.read().split()]).reshape(shape)

