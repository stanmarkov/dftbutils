"""
Some helper functions to speed up visualisation with mayavi2
NOTABENE: ASE and Mayavi must be in the python path
"""
import numpy as np
from mayavi import mlab
from ase.data import covalent_radii
from ase.data.colors import cpk_colors, jmol_colors
from ase.io.pov import get_bondpairs

def showAtoms (atoms, radii=None, colormap=None, resolution=20, 
                 showBonds=False, bonds=None, **kwargs):
    """
    Visualise the atoms as a set of spherical gliphs.
    Each chemical specie results in a separate vtk object added to the
    mayavi pipeline.
    jmol_colors from ASE are used for coloring species.
    covalent_radii * 0.75 are used for sphere scaling if radii not specified.
    resolution of the spheres can also be controled but 20 seems OK.
    if showBonds is True, bonds will be drawn.
    Ideally, bonds should be supplied as 
    [(i_at1, i_at2, np.array([offsetx,offsety,offsetz])),...]
    but routine will try to find them based on get_bondpairs from ase.io.pov.
    **kwargs are directly passed to the bond visualisation routine, mlab.plot3d.
    Suitable kwargs are line_width or tube_radius (and tube_sides).
    KNOWN ISSUE: will fail if non-periodic atomic structure is passed as atoms
                 to solve, introduce check for atoms.get_pbc()
    """
    if colormap is None:
        colormap = jmol_colors
    if radii is None:
        radii = covalent_radii

    # get the unique atomic species
    species = set([(a.number, a.symbol) for a in atoms])
    atomsview = []
    # show different species separately, with different colour
    for atn, ats in species:
        color = tuple(colormap[atn]) 
        scale = 0.75*radii[atn]
        ax,ay,az = zip(*[(at.x,at.y,at.z) for at in atoms if at.number == atn])
        atomsview.append(mlab.points3d(ax,ay,az, color = color,
                         name = '{0}  atoms'.format(ats),
                         scale_factor = scale, mode = 'sphere',
                         resolution = resolution, scale_mode='none'))
    if showBonds:
        cell = [v[i] for i, v in enumerate(atoms.get_cell())]
        bondsview = list()
        if bonds is None:
            bonds = get_bondpairs(atoms)
        for pr in bonds:
            # recall that bondlist is a list of 3-tuples, each tuple being
            # the two indexes of the two bonded atoms, and a vector to the
            # neighbouring cell, if due to periodicity of the atomic structure
            # the 2nd atom is outside the current cell
            # The strategy for visualisation of the bonds is:
            # Get the positions (taking care of atoms in neighbouring cells)
            # Get the middle of the bond
            # draw half tube/line from first atom to middle, with at1's colour
            # if at2 is within principle cell, draw second tube from at2 to the middle
            at1, at2, offset = atoms[pr[0]], atoms[pr[1]], cell*pr[2]
            p1, p2 = at1.position, at2.position + offset
            pm = (p1 + p2)/2.
            halfbond = np.count_nonzero(pr[2]) # nonzero offset means at2 is outside cell
            col1, col2 = tuple(colormap[at1.number]), tuple(colormap[at2.number])
            t1 = [(p1[0], pm[0]), (p1[1], pm[1]), (p1[2], pm[2])]
            bview = list()
            bview.append(mlab.plot3d(*t1, color=col1, **kwargs)) # tube_radius=tuberadius, tube_sides=10)
            if not halfbond:
                t2 = [(p2[0], pm[0]), (p2[1], pm[1]), (p2[2], pm[2])]
                bview.append(mlab.plot3d(*t2, color=col2, **kwargs)) # tube_radius=tuberadius, tube_sides=10)
            bondsview.append(bview)
        return atomsview, bondsview
    return atomsview, None


def showCutPlane(datasource, normal, origin=None,\
                  colormap='RdBu', invertcmap=True,\
                  cmaprange=None, opacity = 1,\
                  contours=None, filledcontours=False,\
                  warped=False, warpscale=10,\
                  name=None, interactive=False):
    """
    Helper function to show a cutplane as either a filled plane or
    a set of contours with support for warping the surface.
    * necessary arguments are datasource (tvtk object), 
      normal (one of 'x','y','z'), and origin (3-tuple center of
      the plane)
    * colormap, cmaprange, invertcmap and opacity define the colours
    * contours must be a list of 1 int and possibly followed by 
      2 floats, defining the number of contours and the range
      of their values; 
      a value of -1 instead of a list resorts to auto-contours
    * warped folds the surface according to warpscale
    * interactive enables the widget to adjust position in real time
    * name is the name of the vtk object that is returned
    """
    directions = {'x':'x_axes','y':'y_axes','z':'z_axes'}
    
    if cmaprange is not None:
        vmin,vmax = cmaprange
    else:
        vmin,vmax = None, None

    if name is None:
        name = 'cutplane-{0}'.format(normal)
    
    cp = mlab.pipeline.scalar_cut_plane(datasource,\
                    plane_orientation=directions[normal],\
                    name = name, vmin=vmin, vmax=vmax,\
                    opacity = opacity, colormap=colormap) 
    
    if origin is not None:
        cp.implicit_plane.origin = origin
    
    if invertcmap:
        cp.module_manager.scalar_lut_manager.reverse_lut = True

    if not interactive:
        cp.implicit_plane.widget.enabled = False

    if contours is not None:
        cp.enable_contours = True
        cp.contour.filled_contours = filledcontours
        if not contours == -1:
            cp.contour.number_of_contours = int(contours[0])
            if len(contours)==3:
                cp.contour.minimum_contour = contours[1]
                cp.contour.maximum_contour = contours[2]
        
    if warped:
        cp.enable_warp_scalar = True
        cp.compute_normals = True
        cp.warp_scalar.filter.scale_factor = warpscale
        
    return cp

