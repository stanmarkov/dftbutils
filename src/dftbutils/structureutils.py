"""
Some routines to convenintly extract parts of the device structure
"""
#from ase.io import read, write
#from ase import Atom, Atoms
from ase.units import Bohr
from ase.calculators.neighborlist import NeighborList
try:
    from pyspglib import spglib as spg
    has_spg = True
except:
    has_spg = False
from ase.data import covalent_radii, atomic_numbers
import numpy as np
import os

LODESTAREXT = "lodestar"

def get_bondpairs(atoms, cutoffs=None, bothways=False, 
                    selfinteraction=False, cutoff_factor=1.1):
    """
    Get a list of bonded atom-pairs.

    Return all pairs of _atoms_ that are apart by less then the sum of
    their _cutoffradii_.

    REMARK: 
        The ase.calculators.neighbourlist is used to build a NeighborList.

    NOTABENE: 
        we call NieghborList with _skin_=0. as the ASE-default 
        of 0.3 is applied to all atoms and significantly confuses the 
        meaning of _cutoffs_.

    Parameters:
        atoms: ASE Atoms object or similar
            The atomic structure.
        cutoffs: dictionary {"Chem.Symbol": float_cutoffradius, }
            A pair of atoms is returned if the distance between the two
            is less than the sum of their cutoffradii.
            No need to specify this for all elements.
            If None, then cutoffradius = cutoff_factor * covalentradius
        cutoff_factor: float
            cutoff_radii = cutoff_factor * covalent_radii[atoms.numbers] # defaults
            The default of 1.1 may suffice in many cases: e.g. Si-Si in diamond-Si
            is 2.35, while rcov_Si is 1.11, so 1.1*rcov_Si*2 > 2.32, but may as
            well be small, depending on analysis we want to make...
        bothways: bool
            Return all pairs, i.e. a1-a2, AND a2-a1.
        selfinteraction: bool
            Return itself as a neighbour, for every atom.

    Return:
        bondpairs: list
            The list contains tupples (at.index, nbrat.index, nbrat.offsets),
            where the offests is a tupple of three integeres showing the 
            offset to neighbour's unit cell, e.g [0,0,0]: nbrat is in atoms,
            while [1,0,0]: nbrat is the next cell along X
    """
    cutoff_radii = cutoff_factor * covalent_radii[atoms.numbers] # defaults
    if cutoffs is not None:
        # a dictionary with radius per chemical element is expected here...
        for chemsymb, rcut in cutoffs.items():
            cutoff_radii[atoms.numbers==atomic_numbers[chemsymb]] = rcut
    nl = NeighborList(cutoffs=cutoff_radii, skin=0.0,
            bothways=bothways, self_interaction=selfinteraction)
    nl.build(atoms)
    bondpairs = []
    for a1 in range(len(atoms)):
        indices, offsets = nl.get_neighbors(a1)
        bondpairs.extend([(a1, a2, offset)
                          for a2, offset in zip(indices, offsets)])
    return bondpairs

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def get_cellvolume(v1, v2=None, v3=None):
    """
    Return the volume enclosed by a unit cell defined by v1, v2, v3
    Permit a single list of three vectors, or three vectors.
    Although the volume is computed as a dotproductcrossporduct,
    the returned number is the absolute value, i.e. always > 0
    """
    if v2 is None:
        assert v3 is None
        return np.abs(np.dot(v1[0],np.cross(v1[1],v1[2])))
    else:
        assert v3 is not None
        return np.abs(np.dot(v1, np.crsocc(v2, v3)))


def get_angle(v1, v2, degrees=True):
    """
    Return the angle between vectors _v1_ and _v2_, in degrees or radians
    """
    uv1 = unit_vector(v1)
    uv2 = unit_vector(v2)
    angle = np.arccos(np.dot(uv1, uv2))
    if degrees:
        return np.rad2deg(angle)
    else:
        return angle


def isInside(pos,grid):
    """
    Check that position is within the domain of the grid
    """
    x,y,z = pos
    if (x >= grid.xmin and x <= grid.xmax) and\
       (y >= grid.ymin and y <= grid.ymax) and\
       (z >= grid.zmin and z <= grid.zmax):
        inside = True
    else:
        inside = False
    return inside


def centerCell (cell,directions=None):
    """
        Center the atoms within the unit cell
        Optionally, select direction(s), e.g.
            ('x') or (0) or ('y','z') or (0,2)
    """
    if directions is None:
        directions = [0,1,2]
        
    for di in directions:
        cc = [a.position[di] for a in cell]
        cmin,cmax = min(cc),max(cc)
        atomicextent = cmax - cmin
        atomiccentre = (cmax+cmin)*.5
#        cellvec = cell.get_cell()[di][di]
        tvec= [0,0,0]
#        tvec[di] = -cmin + (cellvec-atomicextent)/2.
        tvec[di] = -atomiccentre
        cell.translate(tvec)
        
    return cell


def getBoundAtoms(atoms, grid, substitute=[]):
    """
    Assume that atoms is ASE.atoms structure and return the atoms 
    that are within the volume of the specified 3D grid.
    semi-infinite contact with index icont.
    NOTABENE: If atoms is a supercell structure, then the boundatoms
    that is returned by the routine is also a supercell structure,
    whereby the grid defines the cell vectors
    The substitute argument allows substituting chemical elements
    with another one. It should be of the form:
    [ [list_of_chemelem_to_be_substituted], substitution]
    """
    boundatoms=atoms.copy()
    del boundatoms[[at.index for at in atoms if not isInside(at.position,contact_grid)]]

    # substitute chemical species if necessary (e.g. if we've introduced
    # fictitious elements as doping atoms or something
    for at in boundatoms:
        if at.symbol in substitute[0]:
            at.symbol = substitute[1]

    # setting the cell vector in transport direction is probably not
    # best in this way...
    if boundatoms.pbc:
        cellvec = boundatoms.get_cell()
        for i in range(3):
            cellvec[i][i] = grid.extent[i]
        boundatoms.set_cell(cellvec)

    return boundatoms


def report_atoms(atoms, scaledpos=True):
    """
    Print out structure info, assuming it is periodic cell.
    Report lattice constants, angles, and space group, and volume.
    """
    # make sure it's a periodic structure
    assert atoms.get_pbc().all()
    unitcell = atoms.get_cell()
    volume = get_cellvolume(unitcell)
    print "Atoms ({:s}):".format(atoms.get_chemical_formula())
    scaledpositions = atoms.get_scaled_positions()
    for i, at in enumerate(atoms):
        if scaledpos:
            print "{:>5s}: {:>9.5f} {:>9.5f} {:>9.5f}".format(at.symbol, *scaledpositions[i])
        else:
            print "{:>5s}: {:>9.5f} {:>9.5f} {:>9.5f}".format(at.symbol, *at.position)
    print "Unit cell vectors: \n",\
            "\n".join([" ".join(["{:>9.5f}".format(comp) for comp in vec]) 
                       for vec in unitcell])
    print "Lattice sides  (a,b,c)(Angstrom):\n",\
            "{:>9.5f} {:>9.5f} {:>9.5f}".format(
                np.linalg.norm(unitcell[0]), 
                np.linalg.norm(unitcell[1]),
                np.linalg.norm(unitcell[2]))
    print "Lattice angles (alpha(b,c), beta(c,a), gamma(a,b))(Degrees):\n",\
            "{:>9.2f} {:>9.2f} {:>9.2f}".format(
                get_angle(unitcell[1], unitcell[2]), 
                get_angle(unitcell[2], unitcell[0]), 
                get_angle(unitcell[0], unitcell[1]))
    print "Cell volume (Angstrom^3): {:f}".format(volume)
    if has_spg:
        print "Space group: ", spg.get_spacegroup(atoms)


# TODO: change the name of this routine and the argument names.
#       Due to the way it works, it doesn't matter if we're extracting
#       sub-structure of contact or of device region only.
#       The relevant is: input atomic structure and input grid, that
#       possibly binds only a subset of the atoms in the input structure
def get_contact(device, contact_grid, substitute=[]):
    """
    Assume that device is ASE.atoms structure prepared for
    transport simulation and return the atoms that form the
    semi-infinite contact with index icont.
    The routine works by looking eliminating the atoms that
    are outside the contact domain, as obtained from contact_grid.
    Recall that contact grid is present in the simulation directory
    of the contact. It must be obtained first.
    The substitute argument allows substituting chemical elements
    with another one. It should be of the form:
    [ [list_of_chemelem_to_be_substituted], substitution]
    NOTABENE: currently, Z is assumed transport direction
    """
    contact=device.copy()
    del contact[[at.index for at in contact if not isInside(at.position,contact_grid)]]

    # substitute chemical species if necessary (e.g. if we've introduced
    # fictitious elements as doping atoms or something
    for atom in contact:
        if atom.symbol in substitute[0]:
            atom.symbol = substitute[1]

    # setting the cell vector in transport direction is probably not
    # best in this way...
    cellvec = contact.get_cell()
    cellvec[2][2] = contact_grid.zextent
    contact.set_cell(cellvec)

    return contact


def isBondType(elements,atoms,i0,i1):
    """
    Check if a pair of indices _i0_, and _i1_ in _atoms_ corresponsds to
    a pair of chemical _elements_, without significance to the order.
    """
    elements = list(elements)
    chelems = [atoms[i].symbol for i in [i0,i1]]
    if chelems == elements or chelems == list(reversed(elements)):
        return True
    else:
        return False


def getBondLen((a1,a2),atoms,allbonds=None,maxbondlen=5):
    """
    OBSOLETE: DO NOT USE!
    Find the bonds between atom types a0 and a1, such that
    the bond length is no greater than maxbondlen (in Angstr.).
    Optionally, supply allbond pairs, which is better found
    once, since it takes ase some time to build the complete
    list.
    """
    print "Routine getBondLen() of dftbutils/structureutils.py is OBSOLETE. DO NOT USE"
    bondlenlist = []
    bondlist = []
    if allbonds is None:
        allbonds = get_bondpairs(atoms)
    for bond in allbonds:
        i0, i1 = bond[0:2]
        if isBondPair((a1,a2),atoms,i0,i1):
            bondlen = atoms.get_distance(i0,i1,mic=True)
            if bondlen < maxbondlen:
                bondlist.append(bond)
                bondlenlist.append(bondlen)
    return bondlist,bondlenlist

def foldin_atoms(atoms):
    """
    Return a NEW objects (ase.Atoms) containing all atoms
    displaced such that the origin of the coord system is
    in the centre of the unit cell of the Atoms.
    """
    bounds = [(0,atoms.get_cell()[i][i]) 
              for i in range(3)]
    foldedatoms = atoms.copy()
    for at in foldedatoms:
        for i in range(3):
            if at.position[i] < bounds[i][0]:
                at.position[i] += bounds[i][1]
            if at.position[i] > bounds[i][1]:
                at.position[i] += -bounds[i][1]
    return foldedatoms

def get_atomswithin(atoms, xmin=None,xmax=None,ymin=None,
                    ymax=None,zmin=None,zmax=None):
    """
    return the atoms within the volume bound by 
    x|y|zmin and x|y|zmax
    if any of these is omitted, then the natural boundary
    of the atomic structure is taken as a bound in the
    given direction
    """
    ax,ay,az = zip(*[(at.x,at.y,at.z) for at in atoms])
    if xmin is None:
       xmin = min(ax)
    if xmax is None:
       xmax = max(ax)
    if ymin is None:
       ymin = min(ay)
    if ymax is None:
       ymax = max(ay)
    if zmin is None:
       zmin = min(az)
    if zmax is None:
       zmax = max(az)
    return [at for at in atoms if
         xmin <= at.x <= xmax and
         ymin <= at.y <= ymax and
         zmin <= at.z <= zmax]


def get_extent(atoms):
    """
    Find the spacial extent  of *atoms*.
    Return the two points defining the bounding box.
    NOTABENE: Units are Angstroms!
    """
    minx,maxx= min([a.x for a in atoms]), max([a.x for a in atoms])
    miny,maxy= min([a.y for a in atoms]), max([a.y for a in atoms])
    minz,maxz= min([a.z for a in atoms]), max([a.z for a in atoms])
    return (minx, miny, minz), (maxx, maxy, maxz)


def write_lodestar(name, atoms, workdir='./'):
    """
    Write atomic structure for lodestar, assuming that *atoms* is 
    an ase atoms object.
    Write also to .gen (dftb+) for visualisation with ase-gui or 
    e.g. avogadro, mercury, jmol etc.
    """
    atoms.write(os.path.join(workdir, '{0}.{1}'.format(name,'gen')))
    a, b, c = [v[i] for i,v in enumerate(atoms.get_cell())]
    with open(os.path.join(workdir, ".".join(("{}".format(name), LODESTAREXT))), 'w') as fp:
        fp.writelines("!supercell  {}  {}  {}\n".format(a, b, c))
        fp.writelines("$coordinate  natoms={:d}  $end\n".format(len(atoms)))
        for at in atoms:
            fp.writelines("{:d}   {}   {}   {}\n".format(at.number, at.x, at.y, at.z))

def write_povpng(atoms, name, rotation='', sphere_radii=0.4, cutoffs=None,
        canvas_width=640, show_unit_cell=2, tube_radius=0.07, cleanup=True,
        cutoff_factor=1.1):
    """ Write a png-figure of *atoms*, using POVRAY as a rendering engine.

    Parameters:
        atoms : ase.atoms.Atoms object
            Input atomic structure.
        name : string
            Base filename of the output .png figure (and .ini, .pov, which are
            also generated).
        rotation : string
            Rotation string "DegreesAxis[,DegreesAxis[,DegreesAxis]]",
            e.g. rotation = '35x,63y,36z'.
        cutoffs: dictionary {"Chem.Symbol": float_cutoff_radius, }
            A pair of atoms is returned if the distance between the two
            is less than the sum of their cutoff_radii.
            No need to specify this for all elements.
            If None, then cutoff_radius = cutoff_factor * covalent_radius
        cutoff_factor: float
            An alternative (or complementary) to specify cutoff_radii.
            cutoff_radii = cutoff_factor * covalent_radii[atoms.numbers] # defaults
            The default of 1.1 may suffice in many cases: e.g. Si-Si in diamond-Si
            is 2.35, while rcov_Si is 1.11, so 1.1*rcov_Si*2 > 2.32, but may as
            well be small, depending on analysis we want to make...
            NOTABENE: cutoff_factor = 1. for all atoms whose cutoff_radius is
                      specified in cutoffs.
        tube_radius : float
            Radius of the tubes representing bonds; same for all.
        sphere_radii : float, list of floats, or dict of {at.symbol: float_radius}
            Radii for the representation of the atoms as spheres.
            If a single float: use the covalent radius, scaled by the float.
            If a list of floats, then len(radii) == len(atoms) or AssertionError.
            In this case, the value (per atom) is the radius; NO scaling!
            If a dictionary: it should contain a 'Default': scaling_factor, 
            and optionally, 'atom.symbol': scaling_factor, where the
            scaling_factor is applied to the covalent radius of atom.symbol.
            E.g.: {'Si': 0.33, 'O': 0.4, 'Default': 0.4}
            'Default' can be ommitted; 0.4 will be used.
        show_unit_cell : int
            0, 1, or 2 to not show, show, and show all of cell
        canvas_width : int, Default: 640
            Width in pixels of the output PNG; the height is scaled to maintain
            the correct proportions.
        cleanup : bool, Default: True
            remove *.ini and *.pov files.
    """
    
    # find the bonds
    bondatoms = get_bondpairs(atoms, cutoffs=cutoffs, cutoff_factor=cutoff_factor)

    # figure out the radius of the spheres
    _default = 0.4 # scaling factor if default not supplied
    covradii=covalent_radii[atoms.get_atomic_numbers()]
    for at in atoms:
        try:
            # assume that *radii* is a dictionary
            _radii = sphere_radii[at.symbol]*covradii[at.index]
        except KeyError:
            # Suppose we did not specified all chem.elements
            # and may be not even a default.
            _radii = sphere_radii.get('Default', _default)*covradii[at.ix]
        except TypeError:
            # radii is not a Dictionary
            _radii = sphere_radii
            break

    # Create nice-looking image using povray; the write command already runs povray!
    atoms.write('{:s}.pov'.format(name),
         transparent=True, 
         display=False,
         run_povray=True,
         camera_type='perspective',
         canvas_width=canvas_width,
         show_unit_cell=show_unit_cell,
         radii=_radii,
         rotation=rotation,
         bondlinewidth=tube_radius,
         bondatoms=bondatoms,
         )
    if cleanup:
        os.remove('{:s}.ini'.format(name))
        os.remove('{:s}.pov'.format(name))
