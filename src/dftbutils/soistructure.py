"""
Some routines to help with handling SOI-like unit cells
and structures and for building SOI-MOSFET devices.
"""
import logging
import math
from operator import mul
from dftbutils.structureutils import centerCell, get_extent, foldinCell
from ase import Atoms

def get_soiinterfaces(atoms, axis=2):
    """
    Find and return the positions of the:
    * outer interface: as determined by the outermost H atoms
    * top and bottom Si/SiO2 interfaces: as defined by
      O-bridges and the interfacial Si atoms (just below the
      O-bridges).
    Return a dictionary with Htop/bot, Otop/bot, Sitop/bot.
    *axis* should be 0..2 and coincide with interface-normal.
    *atoms* should be an ASE Atoms object.
    """
    # position of outermost Hydrogens
    posHmin = min([at.position[axis] for at in atoms])
    posHmax = max([at.position[axis] for at in atoms])
    # middle of the atoms
    posmiddle = (posHmin + posHmax) / 2.
    # position of the Oxygen bridges at the interface
    Obrtop = min([at.position[axis] for at in atoms if at.symbol=='O' and at.position[axis]>posmiddle])
    Obrbot = max([at.position[axis] for at in atoms if at.symbol=='O' and at.position[axis]<posmiddle])
    # position of the interfacial Si
    Sitop = max([at.position[axis] for at in atoms if at.symbol=='Si' and at.position[axis]<Obrtop])
    Sibot = min([at.position[axis] for at in atoms if at.symbol=='Si' and at.position[axis]>Obrbot])
    return {'Htop': posHmax, 'Hbot': posHmin, 
            'Otop': Obrtop, 'Obot': Obrbot,
            'Sitop': Sitop, 'Sibot': Sibot}


def get_tSOI(atoms, axis=2):
    """
    Find the thickness (in [nm]) of the Si film sandwitched between SiO2.
    Assume that the outermost surfaces are passivated by Hydrogen.
    Currently it assumes symmetric layers, such that middle of the structure
    is in the Si film.
    """
    pp = get_soiinterfaces(atoms, axis=axis)
    posHmax, posHmin = (pp['Htop'], pp['Hbot'])
    Sitop, Sibot = (pp['Sitop'], pp['Sibot'])
    # layer thickness
    tH = posHmax - posHmin
    tSi = abs(Sitop - Sibot)
    tOx_top = abs(posHmax - Sitop)
    tOx_bot = abs(posHmin - Sibot)
    return tSi, tOx_top, tOx_bot, tH


def build_SOI(CML, topOx, botOx, cSi_layer, tML, tvectopox, cellvec, 
                logger=logging.getLogger("SOI factory")):
    """
    Build SOI unit cell from three fragments extracted from a 
    similar unit cell, by repeating the *cSi_layer* for *CML*
    number of times. Note that *cellvec* should be obtained from
    the original reference cell from which the three fragments:
    *topOx*, *botOx*, and *cSi_layer* are obtained.
    NOTABENE: to maintain the validity of cellvec, CML must be 
              multiple of 4 (the number of ML in the unit cell
              of Si) so that we avoid rotation of oxide fragments.
    """
    assert CML%4 == 0, logger.critical("CML must be multiple of 4")
    body = Atoms()
    layer = cSi_layer
    axis = 2   # see below the folding is fixed wrt cellvec 0 and 1
    foldaxes = [dir for dir in range(3) if dir != axis]
    
    body.extend(layer)
    foldcellvec = list(cellvec)

    nML = 1
    odd = False # refers to the layer being added (i.e. newlayer)
    newlayer = layer.copy()
    for i in range(3):
        if i == axis:
            foldcellvec[i] = 0.0
            
    tvec = [0.0,]*3
    tvec[axis] = tML
    # this is rubbish even if it works. must find a more intelligent way.
    while nML < CML:
        nML = nML+1
        if odd:
            newlayer.translate((-cellvec[0]/4, cellvec[1]/4, 0))
        else:
            newlayer.translate((cellvec[0]/4, cellvec[1]/4, 0))
        newlayer.translate(tvec)
        foldinCell(newlayer, axes=foldaxes, cellvec=foldcellvec)
        body.extend(newlayer)
        odd = not odd
        
    assert CML == nML
    tCrystSi = nML*tML 
    logger.debug("Added crystalline Si of {:d} atoms, {:.2f} Angstrom thick ({:d} MLs)".
           format(len(body), tCrystSi, nML))
    
    tvec = [0.0,]*3
    #tvec[axis] = tCrystSi - (math.trunc(tSi/tML))*tML
    tvec[axis] = tvectopox
    topOx.translate(tvec)

    outCell = Atoms()
    for atoms in [topOx, body, botOx]:
        outCell.extend(atoms)

    outCell.set_pbc(True)
    outCell.set_cell(cellvec)

    return centerCell(outCell)


def fix_tSOI(atoms, tSOI, tSiML, ndisSi=[1, 1], axis=2, 
             logger=logging.getLogger("SOI factory")):
    """
    Add or remove core Si atoms from *atoms* to get the desired
    Si thickness *tSOI* in [nm].
    *tSiML* and *tdisSi* are in [Angstroms] and are the monolayer thickness,
    and the thickness of disordered Si around the interface, respectively.
    NOTABENE: internally, the routine uses [Angstroms].
    """
    # find the position of the interfaces
    # ----------------------------------------------------------------------
    logger.info("Rebuilding unit cell to the desired Si body thickness") 
    tSi = tSOI * 10.   # convert to Angstroms
    atoms = centerCell(atoms)
    atoms.write("centered.refcell.gen")
    pp = get_soiinterfaces(atoms, axis=axis)
    posHtop, posHbot =  (pp['Htop'], pp['Hbot'])
    posSitop, posSibot = (pp['Sitop'], pp['Sibot'])
    posOtop, posObot = (pp['Otop'], pp['Obot'])
    posIFtop, posIFbot = (posSitop, posSibot)
    _tSi = posSitop - posSibot
    _tH  = posHtop - posHbot
    _nSilayers = int(round(_tSi/tSiML)) + 1 # number of atomic layers in refcell
    cellvec = [v[i] for i, v in enumerate(atoms.get_cell())]
    tvac = cellvec[axis]-_tH
    unitarea = SOI.aSi*SOI.aSi
    area = reduce(mul, [cellvec[i] for i in range(3) if i != axis], 1)
    nSOIatoms = len([at for at in atoms if 
            at.position[axis] <= posIFtop and at.position[axis] >= posIFbot])
    logger.debug("INPUT unit cell:")
    logger.debug("Position of H  interfacial atoms [Angstr]: {:>12.2f} {:>12.2f}".format(posHtop, posHbot))
    logger.debug("Position of O  interfacial atoms [Angstr]: {:>12.2f} {:>12.2f}".format(posOtop, posObot))
    logger.debug("Position of Si interfacial atoms [Angstr]: {:>12.2f} {:>12.2f}".format(posSitop, posSibot))
    logger.debug("tSOI is {:.2f} [Angstr], "\
            "composed of {:d} layers of Si ({:d} Si atoms)". 
                 format(_tSi,_nSilayers, nSOIatoms))
    logger.debug("Cell vectors [Angstr]: {:>10.2f}{:>10.2f}{:>10.2f}".
            format(*cellvec))
    logger.debug("Vacuum buffer [Angstr]: {:>10.2f}".format(tvac))
    assert nSOIatoms == int(_nSilayers * 2 * area/unitarea)

    # Decide how to fragment the cell and how many layers the SOI needs
    logger.debug("Fragmenting the input unit cell:")
    nextraSilayers = _nSilayers % 4
    n1 = nextraSilayers // 2
    n2 = nextraSilayers // 2 + nextraSilayers % 2
    _textraSilayers = _tSi - tSiML * (_nSilayers - nextraSilayers)
    logger.debug("{:n} extra Si layers will be associated with the top({:n})/bottom({:n}) oxide fragments".
            format(nextraSilayers, n1, n2))
    logger.debug("These Si layers yield {:.2f} [Angstr]".format(_textraSilayers))
    logger.debug("We need to build {:.2f} (or thereby) Angstr of crystalline Si".format(tSi-_textraSilayers))
    tcSi = tSi-_textraSilayers
    CML = int(round(tcSi/(4.*tSiML)))*4
    newtSi = CML*tSiML+_textraSilayers
    logger.debug("The closest multiple of 4 is {:d} layers of Si ({:.2f} Angstr)".format(CML, CML*tSiML))
    logger.debug("Which will yield a total of {:.2f} Angstr of SOI".format(newtSi))

    # Defragment the cell, extracting top and bottom oxide
    # ----------------------------------------------------------------------
    inCell = atoms.copy()
    # extract the two oxides, including the associated Si layers
    # treat the assocated Si layers as disordered Si:
    # these may be thicker or thinner than tSiML, hence the +0.5 below
    # keep in mind that posIFtop/bot corresponds to Si atoms, hence the -1 below.
    tdisSitop = tSiML*(n1 - 1 +0.5)
    tdisSibot = tSiML*(n2 - 1 +0.5)
    topOx = inCell[[a.index for a in inCell if a.position[axis]>(posIFtop-tdisSitop)]]
    botOx = inCell[[a.index for a in inCell if a.position[axis]<(posIFbot+tdisSibot)]]
    logger.debug("Top/Bottom oxide fragment of {:d}/{:d} atoms".format(len(topOx), len(botOx)))

    # isolate the first crystalline layer above the bottom oxide fragment
    layer = Atoms()
    logger.debug("Isolated reference crystalline layer of Si:")
    for atom in inCell[[a.index for a in inCell if 
                     (a.position[axis] > posIFbot+(n2 -1 +0.5)*tSiML and 
                      a.position[axis] < posIFbot+(n2 -1 +1.5)*tSiML)]]:
        layer.extend(atom)
        logger.debug("{:.2f}, {:.2f}, {:.2f}".format(*at.position))
    
    cc = cellvec[axis] + newtSi - _tSi
    cellvec[axis] = cc

    tvectopox = (CML - (_nSilayers - nextraSilayers)) * tSiML
    
    outCell = build_SOI(CML, topOx.copy(), botOx.copy(), layer.copy(), tSiML, tvectopox, cellvec)    

    return outCell



class SOI(object):

    """
    Embodyment of the SOI atomic model.
    NOTABENE: All dimensional parameters are in [nm]
              All doping related parameters are in [cm-3]
              x is transport direction (source-to-drain)
              y is periodic
              z is normal to the SiO2 interface
    """

    devpars = ["tSOI", "Lg", "Wg", "Lsp", "Lext", "EOT", "tBOX", "aSi"] # [nm], except aSi [Angstr]
    tSOItol = 0.05 # [nm], i.e. 0.5 Angstr.
    kox = 3.9
    aSi = 5.43     # [Angstr]
    tSiML = aSi/4.   # [Angstr]
    
    def __init__(self, refcell, **kwargs):

        if 'logg er' not in kwargs.keys():
            logger = logging.getLogger("SOI factory")
        else:
            logger = kwargs['logger']

        for key in kwargs.keys():
            if key in self.devpars:
                setattr(self, key, kwargs[key])
            else:
                if key != 'logger':
                    logger.warn("Unrecognised device parameter: {:s}".format(key))

        # NOTABENE: the return values are in Angstrom, hence the /10.
        _tSOI, _tOxtop, _tOxbot, _tH = [tt / 10. for tt in get_tSOI(refcell)]
        logger.debug("tSOI of refcell is {:.3f} nm".format(_tSOI))
        logger.debug("tOxtop, tOxbot and tH are {:.2f} {:.2f} {:.2f} nm".
                format(_tOxtop, _tOxbot, _tH))

        if abs(self.tSOI - _tSOI) <= self.tSOItol:
            unitcell = refcell.copy()
        else:
            unitcell = fix_tSOI(refcell, self.tSOI, self.tSiML, axis=2)
            _tSOI, _tOxtop, _tOxbot, _tH = [tt / 10. for tt in get_tSOI(unitcell)]
        self.tSOI = _tSOI
        logger.info("tSOI is {:.2f} nm".format(self.tSOI))
            

        Ldev = self.Lg + 2*self.Lsp + 2*self.Lext

        # note the / 10. below, since Ldev is in nm, but get_cell() returns angstroms
        ncells = int(math.ceil(Ldev / (unitcell.get_cell()[0][0] / 10.) ))
        logger.info("Unitcell will be repeated {:d} times".format(ncells))

        self.device = centerCell(unitcell.repeat((ncells, 1, 1)))
        self.Ldev = self.device.get_cell()[0][0]
        logger.info("Ldev is {:.2f} nm".format(self.Ldev))
        logger.info("Device consists of {:d} atoms".format(len(self.device)))
        p1, p2 = get_extent(self.device)
        logger.info("Device atoms extent in x/y/z in [nm]:") 
        logger.info("{:>12.2f}{:>12.2f}{:>12.2f}".format(p1[0]/10., p1[1]/10., p1[2]/10.))
        logger.info("{:>12.2f}{:>12.2f}{:>12.2f}".format(p2[0]/10., p2[1]/10., p2[2]/10.))

        # fix cell in z, so that we have the necessary vacuum buffer to emulate the thick BOX.
        # tBOX = toxbot + EOTvacbot
        EOTvacbot = self.tBOX - _tOxbot
        tvacbot = EOTvacbot / self.kox
        # recall that the cell info is in Angstrom, hence the *10
        cc = (0, 0, (_tH + 2 * tvacbot)* 10.)
        logger.info('Given BOX of {:.2f} [nm], the vacuum buffer is {:.2f} [nm]'.
            format(self.tBOX, 2*tvacbot))
        cell = self.device.get_cell()
        cell[-1] = cc
        self.device.set_cell(cell)
        self.device.set_pbc(True)

        # report also the position of the top gate.
        top  = p2[-1]/10.
        tvactop = (self.EOT -_tOxtop)/self.kox # [nm]
        logger.debug("Vacuum buffer between atoms top and gate is {:.2f} nm".format(tvactop))
        posGate = tvactop + top # [nm]
        logger.info('Given EOT of {0:.2f} [nm], the position of the gate is Z = {1:.2f} [nm]'.format(self.EOT,posGate))



