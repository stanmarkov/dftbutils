#!/usr/bin/env python
from distutils.core import setup

import src as dftbutils

setup(name='dftbutils',
      version=dftbutils.__revision__,
      description='A library for pre-/post-processing, analysis and visualisation of data input/output to/from dftb+ or lodestar',
      long_description=open('README.txt').read(),
      author=dftbutils.__author__,
      author_email=dftbutils.__email__,
      url='https://bitbucket.org/barnacles/dftbutils',
      package_dir={'': 'src', },
      packages=['dftbutils', ],
      scripts=['bin/devicefactorysoi', 'bin/dbcbuild', 'bin/gen2lodestar'],
      platforms=['any'],
      keywords=['dftb', 'dftb+', 'lodestar', 'dftbutils'],
      license='MIT',
      classifiers=[
        'Development Status :: 0 - initial/exploratory',
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'Intended Audience :: Science/Research',
        'Environment :: Console',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development',
        ],
)

