#!/usr/bin/env python2.7
import sys
import os
import argparse
import numpy as np
from dftbutils.general import setuplogger
from dftbutils.poissonbc import Gate, write_DBC


if __name__ == "__main__":
    
    # argument parsing 
    # -------------------------------------------------------------------
    logger = setuplogger("./", "DBCbuild")

    # argument parsing 
    # -------------------------------------------------------------------
    parser = argparse.ArgumentParser(
            description="""Tool for creating a file with the Dirichlet BC for a
            Poisson box, by specifying 'metallic gates'"""
            )
    parser.add_argument(
            "-g", "--gatetype", dest="gatetype", type=str, default='planar', 
            action="store",
            help="Type of gate, one of 'planar'... the only one currently supported"
            )
    parser.add_argument(
            "-d", "--dimensions", nargs='*', type=float, default=None, 
            action="store",
            help="Gate dimensions (L, H, W), as 0 to 3 floats: [length [[height] [width]]]."
            )
    parser.add_argument(
            "-p", "--position", nargs='*', action="store",
            help="3-tuple reference position from which the gate expands "
            "along the normal by H, longitudinally by -/+(L/2)"
            )
    parser.add_argument(
            "-v", "--potential", type=float, action="store", default=0.0,
            help="Value of the potential [V]"
            )
    parser.add_argument(
            "-n", "--normal", type=str, default='z', 
            help="Normal direction of the gate (H)"
            )
    parser.add_argument(
            "-l", "--longitudinal", type=str, default='x', 
            help="Longitudinal direction of the gate (L)"
            )
    parser.add_argument(
            "--gridfile", dest="gridfile", type=str, default='grid.dat', 
            action="store",
            help="Grid file defining the poisson box with the following content:\n"
            "nx, ny, nz\n"
            "x1, y1, z1\n"
            "x2, y2, z2")
    parser.add_argument(
            "-a", "--append", action="store_true",
            help="if specified, the tool appends the current gate to an existing file"
            )
    parser.add_argument(
            "-out", dest="name", type=str, default="dbc.dat", 
            action="store",
            help="Name of output file."
            )
    args = parser.parse_args()

    # Get the Poisson box from a run by lodestar
    gridfile = args.gridfile
    gridinfo = np.loadtxt(gridfile)
    poisson_npts = [int(f) for f in gridinfo[0]]
    poisson_box = (gridinfo[1], gridinfo[2])

    ldim = ['L', 'H', 'W']
    dimensions = {}
    if args.dimensions is not None:
        for i, d in enumerate(args.dimensions):
            dimensions[ldim[i]] = float(d)

    if not len(args.position):
        log.critical("position is a mandatory argument")
        sys.exit()
    position = [float(p) for p in args.position]

    gate = Gate(poisson_box, poisson_npts, gatetype=args.gatetype,
            dimensions=dimensions, position=position, 
            dirl=args.longitudinal, dirn=args.normal,
            logger=logger)
    gate.potential = args.potential
    write_DBC(poisson_box, poisson_npts, [gate,], args.name, append=args.append)

#    # example of top gate along transport direction (x)
#    g1 = Gate(poisson_box, poisson_npts, gatetype='planar', 
#            dimensions={'L':30,}, position=(0, 0, 16), logger=logger)
#
#    # example of ground plane
#    # careful with the definition of 'position' for the ground plane,
#    # as it sould capture only the bottom-most points of the device grid
#    g2 = Gate(poisson_box, poisson_npts, gatetype='planar', 
#            dimensions={}, position=(0, 0, -37.0), dirn='-z', logger=logger)
#
#    # example of some ridiculous burried side gate with offset from the centre; why not?
#    g3 = Gate(poisson_box, poisson_npts, gatetype='planar',
#            dimensions={'L':10, 'H':0.01}, position = (0, 2.7, 10), dirl='-z', dirn='-y', logger=logger)
#
#    # top gate has variable Vgs
#    g1.potential = 1.0
#    # ground plane would typically be at 0.0, but there may be a build-in potential difference.
#    # also, it may be contacted and operated as a second, independent gate.
#    g2.potential = 0.0

#    #write_DBC(poisson_box, poisson_npts, [g1], "./Vgs0.0--topgate.dat")
#    #write_DBC(poisson_box, poisson_npts, [g2], "./Vgs0.0--groundplane.dat")
#    write_DBC(poisson_box, poisson_npts, [g1, g2], "./Vgs0.0.dat")

